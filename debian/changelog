smtube (19.6.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Alf Gaida ]
  * Bumped debhelper-compat to 12, no changes needed
  * Bumped Standards-Version to 4.4.0, no changes needed
  * Bumped years in copyright to 2019
  * Refreshed hardening.patch

 -- Alf Gaida <agaida@siduction.org>  Mon, 19 Aug 2019 22:15:43 +0200

smtube (18.9.0-1) unstable; urgency=medium

  * New upstream version 18.9.0.
  * Added myself to uploaders.
  * Bumped Standard-Version to 4.2.1, no changes needed
  * Bumped Compat to 11
  * Bumped dh to >= 11~
  * Fixed Hompage
  * Added debian/upstream/metadata - don't be upset, it is svn, so it is
    quite normal that smtube is a path within smplayer, same for the shared
    Bugtracker.
  * Compress with xz
  * Set language to C.UTF-8 in rules
  * Linker flags added, only link when needed.

 -- Alf Gaida <agaida@siduction.org>  Sat, 10 Nov 2018 21:06:25 +0100

smtube (18.3.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ James Cowgill ]
  * New upstream version 18.3.0
    - Preferred Quality setting was fixed. (Closes: #797498)
    - SMTube can't find video URL was fixed. (Closes: #797500)

  [ Pavel Rehak ]
  * Update debian/copyright
  * Add dragonplayer as optional dependency.
  * Debhelper: 10
  * Add manpage
  * Update hardening.patch
  * Standards-Version: 4.1.4
  * Add SMTube source to watch file
  * Add uploader
  * Remove mplayer2 and gnome-mplayer as optional dependency.
  * Update package description.

 -- Pavel Rehak <pavel-rehak@email.cz>  Wed, 27 Jun 2018 19:39:11 +0200

smtube (15.5.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream release [May 2015] (Closes: #785142).
  * Switch to QT5.
  * Build-Depends:
    - libqt4-dev (>= 4.3)
    + libqt5webkit5-dev
    + qttools5-dev-tools
    + qtscript5-dev
  * Added new "hardening.patch".
  * rules: remove upstream Changelog, if empty.
  * Updated Vcs-Browser URL.
  * Standards-Version: 3.9.6.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 19 May 2015 19:38:34 +1000

smtube (14.8.0-1) unstable; urgency=medium

  * New upstream release.

 -- Alessio Treglia <alessio@debian.org>  Thu, 11 Sep 2014 16:12:53 +0100

smtube (2.1-2) unstable; urgency=medium

  * Add mpv as optional dependency. (Closes: #752590)

 -- Alessio Treglia <alessio@debian.org>  Fri, 11 Jul 2014 16:13:26 +0100

smtube (2.1-1) unstable; urgency=low

  * New upstream release.
  * Fix VCS field. (Closes: #722434)
  * Update copyright holders of debian/* files.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Sun, 30 Mar 2014 19:03:04 +0100

smtube (1.8-1) unstable; urgency=low

  * Upload to unstable.
  * New upstream release.

 -- Alessio Treglia <alessio@debian.org>  Thu, 03 Oct 2013 17:16:56 +0100

smtube (1.6-1) experimental; urgency=low

  * New upstream release.
  * Update debian/copyright.

 -- Alessio Treglia <alessio@debian.org>  Sat, 13 Apr 2013 00:30:34 +0200

smtube (1.5-1) experimental; urgency=low

  * Initial release. (Closes: #700493)

 -- Alessio Treglia <alessio@debian.org>  Wed, 13 Feb 2013 11:48:44 +0000
